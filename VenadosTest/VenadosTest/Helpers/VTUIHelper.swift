//
//  VTUIHelper.swift
//  VenadosTest
//
//  Created by Ingeniero de Software on 5/11/19.
//  Copyright © 2019 Dacode. All rights reserved.
//

import UIKit

class VTUIHelper: NSObject {
    
    final func stringToDate (date: String) -> Date{
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: Locale.current.identifier)
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        formatter.timeZone = NSTimeZone(name: "UTC")! as TimeZone
        let formatterDate = formatter.date(from: date)
        return formatterDate!
    }
    
    func dayOfTheWeek(day : Int) -> String? {
        let weekdays = [
            "DOM",
            "LUN",
            "MAR",
            "MIE",
            "JUE",
            "VIE",
            "SAB"
        ]
        return weekdays[day - 1]
    }
}
