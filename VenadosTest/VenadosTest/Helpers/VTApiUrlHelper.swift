//
//  VTApiUrlHelper.swift
//  VenadosTest
//
//  Created by Ingeniero de Software on 5/11/19.
//  Copyright © 2019 Dacode. All rights reserved.
//

import UIKit

final class VTApiUrlHelper: NSObject {
    
    public static var shared: VTApiUrlHelper = {
        struct Static {
            static let instance = VTApiUrlHelper()
        }
        return Static.instance
    }()
    
    private let apiProtocol = "https://"
    private let apiHost =  "venados.dacodes.mx/"
    private let apiVirtualPath = "/api"
    private (set) var apiUrl: String!
    
    // MARK: - Initialization
    private override init() {
        apiUrl = apiProtocol + apiHost  + apiVirtualPath
    }
    
    //To path images
    public var path : String {
        return self.apiProtocol + self.apiHost
    }
    
    public var urlGetGames : String {
        return self.apiUrl + "/games"
    }
    
    public var urlGetStadistics : String {
        return self.apiUrl + "/statistics"
    }
    
    public var urlGetPlayers : String {
        return self.apiUrl + "/players"
    }
}
