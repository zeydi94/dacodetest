//
//  VTStadisticModel.swift
//  VenadosTest
//
//  Created by Ingeniero de Software on 5/14/19.
//  Copyright © 2019 Dacode. All rights reserved.
//

import UIKit

class VTStadisticModel: NSObject {
    var goals: Int?
    var image: String?
    var team: String?
    var points: Int?
    var jj: Int?
    var dg: Int?
    
    init(goals: Int?,image: String?,team: String?,points: Int?,jj: Int?,dg: Int?) {
        self.goals = goals
        self.image = image
        self.team = team
        self.points = points
        self.jj = jj
        self.dg = dg
    }
}
