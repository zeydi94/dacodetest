//
//  VTGameModel.swift
//  VenadosTest
//
//  Created by Ingeniero de Software on 5/11/19.
//  Copyright © 2019 Dacode. All rights reserved.
//

import UIKit

class VTGameModel: NSObject {
    var awayScore: Int?
    var dateTime: Date?
    var homeScore: Int?
    var image: String?
    var league: String?
    var local: Int?
    var opponent: String?
    var opponentImage: String?
    
    init(awayScore: Int?,dateTime: Date?,homeScore: Int?,image: String?,league: String?,local: Int?,opponent: String?,opponentImage: String?) {
        
        self.awayScore = awayScore
        self.dateTime = dateTime
        self.homeScore = homeScore
        self.image = image
        self.league = league
        self.local = local
        self.opponent = opponent
        self.opponentImage = opponentImage
    }
}
