//
//  VTPlayerModel.swift
//  VenadosTest
//
//  Created by Ingeniero de Software on 5/14/19.
//  Copyright © 2019 Dacode. All rights reserved.
//

import UIKit

class VTPlayerModel: NSObject {
    var name: String?
    var firstSurname: String?
    var secondSurname: String?
    var position : String?
    var birthday : Date?
    var birthPlace : String?
    var weight : Double?
    var height: Double?
    var lastTeam: String?
    var image: String?
    
    
    init(name: String?,firstSurname: String?,secondSurname: String?,position : String?,birthday : Date?,birthPlace : String?,weight : Double?,height: Double?,lastTeam: String?, image: String?) {
        self.name = name
        self.firstSurname = firstSurname
        self.secondSurname = secondSurname
        self.position = position
        self.birthday = birthday
        self.birthPlace = birthPlace
        self.weight = weight
        self.height = height
        self.lastTeam = lastTeam
        self.image = image
        
    }
}
