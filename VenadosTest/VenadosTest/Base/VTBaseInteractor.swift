//
//  VTBaseInteractor.swift
//  VenadosTest
//
//  Created by Ingeniero de Software on 5/11/19.
//  Copyright © 2019 Dacode. All rights reserved.
//

import UIKit
import Alamofire

class VTBaseInteractor: NSObject {
    func getHeaders() -> HTTPHeaders {
        let headers: HTTPHeaders = [
            "Accept": "application/json"
        ]
        return headers
        
    }
}
