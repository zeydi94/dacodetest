//
//  VTSideMenuTableViewController.swift
//  VenadosTest
//
//  Created by Ingeniero de Software on 5/11/19.
//  Copyright © 2019 Dacode. All rights reserved.
//

import UIKit
import SideMenu

class VTSideMenuTableViewController: UITableViewController {
    
    @IBOutlet weak var viewProfile: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)
        //Home
        if cell?.tag == 100{
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    private func setupView(){
        //Header
        self.viewProfile.layer.cornerRadius = self.viewProfile.frame.size.width / 2
        self.viewProfile.layer.masksToBounds = false
        self.viewProfile.layer.borderWidth = 1
        self.viewProfile.layer.borderColor = UIColor.white.cgColor
        self.viewProfile.clipsToBounds = true
    }
}
