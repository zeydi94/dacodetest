//
//  VTCopaViewController.swift
//  VenadosTest
//
//  Created by Ingeniero de Software on 5/11/19.
//  Copyright © 2019 Dacode. All rights reserved.
//

import UIKit

class VTCopaViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tvcGames: UITableView!
    let interactor = VTHomeInteractor()
    var refreshControl = UIRefreshControl()
    var gamesArray = [VTGameModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tvcGames.delegate = self
        self.tvcGames.dataSource = self
        self.getGames()
        self.refreshControl.addTarget(self, action: #selector(self.refresh), for: UIControl.Event.valueChanged)
        self.tvcGames.addSubview(self.refreshControl)
    }
    
    @objc func refresh(){
        self.getGames()
        self.refreshControl.endRefreshing()
    }
    
    private func getGames(){
        self.interactor.getGames(success: self.onSuccesGames, errorResponse: self.onError)
    }
    
    private func onSuccesGames(games : [VTGameModel]){
        DispatchQueue.main.async {
            self.gamesArray = games.filter {
                $0.league == "Copa MX"
            }
            self.tvcGames.reloadData()
        }
    }
    
    private func onError(error: String){
        let alert = UIAlertController(title: "Error", message: "Unexpected error", preferredStyle: .alert)
        self.present(alert, animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return gamesArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let game = self.gamesArray[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellGame", for: indexPath) as! VTGameCopaTableViewCell
        cell.configureView(game: game)
        cell.parent = self
        return cell
    }
}
