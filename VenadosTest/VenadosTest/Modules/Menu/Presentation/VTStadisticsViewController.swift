//
//  VTStadisticsViewController.swift
//  VenadosTest
//
//  Created by Ingeniero de Software on 5/14/19.
//  Copyright © 2019 Dacode. All rights reserved.
//

import UIKit

class VTStadisticsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tvcStadistic: UITableView!
    var interactor = VTStadisticInteractor()
    var stadisticArray = [VTStadisticModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tvcStadistic.delegate = self
        self.tvcStadistic.dataSource = self
        self.tvcStadistic.estimatedRowHeight = 88
        self.tvcStadistic.rowHeight = UITableView.automaticDimension
        self.getStadistics()
    }
    
    private func getStadistics(){
        self.interactor.getStadistics(success: self.onSuccesStadistics, errorResponse: self.onError)
    }
    
    private func onSuccesStadistics(stadistics : [VTStadisticModel]){
        DispatchQueue.main.async {
            self.stadisticArray = stadistics
            self.tvcStadistic.reloadData()
        }
    }
    
    private func onError(error: String){
        let alert = UIAlertController(title: "Error", message: "Unexpected error", preferredStyle: .alert)
        self.present(alert, animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return stadisticArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let stadistic = stadisticArray[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellStadistic", for: indexPath) as! VTStadisticTableViewCell
        cell.configureItem(stadistic: stadistic)
        return cell
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 50))
        //Title
        let lblTitle = UILabel(frame: CGRect(x: 10, y: 0, width: 150 , height: 25))
        lblTitle.text = "Tabla General"
        lblTitle.textColor = UIColor.white
        lblTitle.font = UIFont.boldSystemFont(ofSize: 17)
        //JJ
        let lblJj = UILabel(frame: CGRect(x: tableView.frame.width - 150, y: 0, width: 50 , height: 25))
        lblJj.text = "JJ"
        lblJj.textColor = UIColor.white
        lblJj.font = UIFont.boldSystemFont(ofSize: 17)
        lblJj.textAlignment = .center
        
        //DG
        let lblDg = UILabel(frame: CGRect(x: tableView.frame.width - 100, y: 0, width: 50 , height: 25))
        lblDg.text = "DG"
        lblDg.textColor = UIColor.white
        lblDg.font = UIFont.boldSystemFont(ofSize: 17)
        lblDg.textAlignment = .center
        
        //Points
        let lblPts = UILabel(frame: CGRect(x: tableView.frame.width - 50, y: 0, width: 50 , height: 25))
        lblPts.text = "DG"
        lblPts.textColor = UIColor.white
        lblPts.font = UIFont.boldSystemFont(ofSize: 17)
        lblPts.textAlignment = .center
    
        view.backgroundColor = UIColor.darkGray
        view.addSubview(lblTitle)
        view.addSubview(lblJj)
        view.addSubview(lblDg)
        view.addSubview(lblPts)
        return view
    }
}
