//
//  VTStadisticTableViewCell.swift
//  VenadosTest
//
//  Created by Ingeniero de Software on 5/14/19.
//  Copyright © 2019 Dacode. All rights reserved.
//

import UIKit
import SDWebImage

class VTStadisticTableViewCell: UITableViewCell {

    @IBOutlet weak var imgIco: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblDg: UILabel!
    @IBOutlet weak var lblPts: UILabel!
    @IBOutlet weak var lblJj: UILabel!
    @IBOutlet weak var lblNumber: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configureItem(stadistic: VTStadisticModel){
        lblNumber.text = String(stadistic.goals!)
        imgIco.sd_setImage(with: URL(string: stadistic.image!), placeholderImage: UIImage(named: "ic_venadosfc.png"))
        lblName.text = stadistic.team
        lblDg.text = String(stadistic.dg!)
        lblPts.text = String(stadistic.points!)
        lblJj.text = String(stadistic.jj!)
    }
}
