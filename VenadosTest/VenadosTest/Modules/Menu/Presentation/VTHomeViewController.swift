//
//  VTHomeViewController.swift
//  VenadosTest
//
//  Created by Ingeniero de Software on 5/11/19.
//  Copyright © 2019 Dacode. All rights reserved.
//

import UIKit
import SideMenu

class VTHomeViewController: UIViewController {
    @IBOutlet weak var containerAscenso: UIView!
    @IBOutlet weak var containerCopa: UIView!
    @IBOutlet weak var segmentOptions: UISegmentedControl!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupSideMenu()
    }
    
    @IBAction func onClickSegment(_ sender: Any) {
        if segmentOptions.selectedSegmentIndex == 0{
            containerCopa.isHidden = false
            containerAscenso.isHidden = true
        }
        if segmentOptions.selectedSegmentIndex == 1{
            containerCopa.isHidden = true
            containerAscenso.isHidden = false
        }
    }
    
    fileprivate func setupSideMenu() {
        // Define the menus
        SideMenuManager.default.menuLeftNavigationController = storyboard!.instantiateViewController(withIdentifier: "LeftMenuNC") as? UISideMenuNavigationController
        SideMenuManager.default.menuAddPanGestureToPresent(toView: self.navigationController!.navigationBar)
        SideMenuManager.default.menuAddScreenEdgePanGesturesToPresent(toView: self.navigationController!.view, forMenu: UIRectEdge.left)
        SideMenuManager.default.menuFadeStatusBar = false
        SideMenuManager.default.menuShadowRadius = 1
        SideMenuManager.default.menuWidth = self.view.frame.width - 90
    }
}

extension VTHomeViewController: UISideMenuNavigationControllerDelegate {
    
    func sideMenuWillAppear(menu: UISideMenuNavigationController, animated: Bool) {
    }
    
    func sideMenuDidAppear(menu: UISideMenuNavigationController, animated: Bool) {
    }
    
    func sideMenuWillDisappear(menu: UISideMenuNavigationController, animated: Bool) {
    }
    
    func sideMenuDidDisappear(menu: UISideMenuNavigationController, animated: Bool) {
    }
}

