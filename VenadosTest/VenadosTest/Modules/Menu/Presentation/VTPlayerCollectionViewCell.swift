//
//  VTPlayerCollectionViewCell.swift
//  VenadosTest
//
//  Created by Ingeniero de Software on 5/14/19.
//  Copyright © 2019 Dacode. All rights reserved.
//

import UIKit

class VTPlayerCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imgIco: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    
    func configureItem(player: VTPlayerModel){
        self.imgIco.layer.cornerRadius = self.imgIco.frame.size.width / 2
        self.imgIco.layer.masksToBounds = false
        self.imgIco.clipsToBounds = true
        self.imgIco.sd_setImage(with: URL(string: player.image!), placeholderImage: UIImage(named: "ic_venadosfc.png"))
        self.lblName.text = "\(player.position!)\n\(player.name!) \(player.firstSurname!)"
        
    }
}
