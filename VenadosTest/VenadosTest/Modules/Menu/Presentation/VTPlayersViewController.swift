//
//  VTPlayersViewController.swift
//  VenadosTest
//
//  Created by Ingeniero de Software on 5/14/19.
//  Copyright © 2019 Dacode. All rights reserved.
//

import UIKit

class VTPlayersViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    @IBOutlet weak var cvPlayers: UICollectionView!
    let interactor = VTPlayerInteractor()
    var playersArray = [VTPlayerModel]()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        self.cvPlayers.delegate = self
        self.cvPlayers.dataSource = self
        self.getPlayers()
    }
    
    private func getPlayers(){
        self.interactor.getPlayers(success: onSuccessPlayers, errorResponse: onError)
    }
    
    private func onSuccessPlayers(players : [VTPlayerModel]){
        DispatchQueue.main.async {
            self.playersArray = players
            self.cvPlayers.reloadData()
        }
    }
    
    private func onError(error: String){
        let alert = UIAlertController(title: "Error", message: "Unexpected error", preferredStyle: .alert)
        self.present(alert, animated: true, completion: nil)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return playersArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let player = playersArray[indexPath.row]
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellPlayer", for: indexPath) as! VTPlayerCollectionViewCell
        cell.configureItem(player: player)
        return cell
    }
}
