//
//  VTGameCopaTableViewCell.swift
//  VenadosTest
//
//  Created by Ingeniero de Software on 5/11/19.
//  Copyright © 2019 Dacode. All rights reserved.
//

import UIKit
import SDWebImage
import EventKit

class VTGameCopaTableViewCell: UITableViewCell {
    @IBOutlet weak var imgCalendar: UIImageView!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var imgLogoHomeScore: UIImageView!
    @IBOutlet weak var lblNameHome: UILabel!
    @IBOutlet weak var lblScore: UILabel!
    @IBOutlet weak var imgLogoOpponent: UIImageView!
    @IBOutlet weak var lblNameOpponent: UILabel!
    var game : VTGameModel?
    var parent : VTCopaViewController?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configureView(game: VTGameModel){
        self.game = game
        let tap = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        self.imgCalendar.isUserInteractionEnabled = true
        self.imgCalendar.addGestureRecognizer(tap)
        
        let calendar = Calendar(identifier: .gregorian)
        let components = calendar.dateComponents([.year, .month, .day, .hour, .weekday], from: game.dateTime!)
        self.lblDate.text = "\(components.day!) \(VTUIHelper().dayOfTheWeek(day: components.weekday!)!)"
        self.imgLogoHomeScore.image = UIImage.init(named: "ic_venadosfc")
        self.lblNameHome.text = "Venados F.C."
        self.lblScore.text = "\(game.homeScore!)-\(game.awayScore!)"
        self.imgLogoOpponent.sd_setImage(with: URL(string: game.opponentImage!), placeholderImage: UIImage(named: "ic_venadosfc.png"))
        self.lblNameOpponent.text = game.opponent
    }
    
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        self.saveGameInCalendar(game: game!)
    }
    
    private func saveGameInCalendar(game: VTGameModel){
        let eventStore : EKEventStore = EKEventStore()
        eventStore.requestAccess(to: .event) { (granted, error) in
            if (granted) && (error == nil) {
                
                let event:EKEvent = EKEvent(eventStore: eventStore)
                event.title = "Venados fc vs \(game.opponent!)"
                
                event.startDate = game.dateTime!
                event.endDate = game.dateTime!
                event.notes = "Sin notas"
                event.calendar = eventStore.defaultCalendarForNewEvents
                do {
                    try eventStore.save(event, span: .thisEvent)
                    //Fecha que se abrirá en el calendario
                    let date : NSDate = game.dateTime! as NSDate
                    self.gotoAppleCalendar(date: date)
                    
                } catch _ as NSError {
                    let alert = UIAlertController(title: "Error", message: "Unexpected error", preferredStyle: .alert)
                    self.parent!.present(alert, animated: true, completion: nil)
                }
            }
            else{
                let alert = UIAlertController(title: "Error", message: "Unexpected error", preferredStyle: .alert)
                self.parent!.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    func gotoAppleCalendar(date: NSDate) {
        let interval = date.timeIntervalSinceReferenceDate
        let url = NSURL(string: "calshow:\(interval)")!
        UIApplication.shared.open(url as URL, options: [:], completionHandler:  nil)
    }
}
