//
//  VTHomeInteractor.swift
//  VenadosTest
//
//  Created by Ingeniero de Software on 5/11/19.
//  Copyright © 2019 Dacode. All rights reserved.
//

import Foundation
import Alamofire

class VTHomeInteractor: VTBaseInteractor {
    
    public func getGames(success: @escaping(([VTGameModel]) -> Void), errorResponse: @escaping (String) -> Void){
        let url  = VTApiUrlHelper.shared.urlGetGames
        let headers = self.getHeaders()
        
        Alamofire.request(url, method: .get, headers: headers).responseJSON { response in
            switch (response.result){
            case .success(let json):
                let statusCode = response.response?.statusCode
                let dictionary = json as? NSDictionary

                if statusCode == 200 {
                    let games = self.dictToGameArrayModel(dictionary!)
                    success(games)
                }else {
                    return errorResponse("Error")
                }
            case .failure(let error):
                errorResponse(error.localizedDescription)
            }
        }
    }
    
    private func dictToGameArrayModel(_ dict : NSDictionary) -> [VTGameModel] {
        var array = [VTGameModel]()
        let dictData = dict["data"] as? NSDictionary
        let arrayGames = dictData!["games"] as? NSArray
        for game in arrayGames! {
            if let dict = game as? NSDictionary {
                array.append(self.dictToGameModel(dict))
            }
        }
        return array
    }
    
    func dictToGameModel(_ dict : NSDictionary) -> VTGameModel {
        var dateTime = Date()
        if let dateTimeStr = dict["datetime"] as? String {
            dateTime = VTUIHelper().stringToDate(date: dateTimeStr)
        }
        
        let gameModel = VTGameModel(awayScore:  dict["away_score"] as? Int, dateTime: dateTime, homeScore:  dict["home_score"] as? Int, image: dict["image"] as? String, league: dict["league"] as? String, local: dict["local"] as? Int, opponent: dict["opponent"] as? String, opponentImage:dict["opponent_image"] as? String )
       
        return gameModel
    }
}

