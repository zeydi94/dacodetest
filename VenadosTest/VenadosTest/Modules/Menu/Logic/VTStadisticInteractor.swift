//
//  VTStadisticInteractor.swift
//  VenadosTest
//
//  Created by Ingeniero de Software on 5/14/19.
//  Copyright © 2019 Dacode. All rights reserved.
//

import UIKit
import Alamofire

class VTStadisticInteractor: VTBaseInteractor {
    
    public func getStadistics(success: @escaping(([VTStadisticModel]) -> Void), errorResponse: @escaping (String) -> Void){
        let url  = VTApiUrlHelper.shared.urlGetStadistics
        let headers = self.getHeaders()
        
        Alamofire.request(url, method: .get, headers: headers).responseJSON { response in
            switch (response.result){
            case .success(let json):
                let statusCode = response.response?.statusCode
                let dictionary = json as? NSDictionary
                
                if statusCode == 200 {
                    let stadistics = self.dictToGameArrayModel(dictionary!)
                    success(stadistics)
                }else {
                    return errorResponse("Error")
                }
            case .failure(let error):
                errorResponse(error.localizedDescription)
            }
        }
    }
    
    private func dictToGameArrayModel(_ dict : NSDictionary) -> [VTStadisticModel] {
        var array = [VTStadisticModel]()
        let dictData = dict["data"] as? NSDictionary
        let arrayStadistics = dictData!["statistics"] as? NSArray
        for stadistic in arrayStadistics! {
            if let dict = stadistic as? NSDictionary {
                array.append(self.dictToStadisticModel(dict))
            }
        }
        return array
    }
    
    func dictToStadisticModel(_ dict : NSDictionary) -> VTStadisticModel{
        
        let stadisticModel = VTStadisticModel(goals: dict["a_goals"] as? Int, image: dict["image"] as? String, team: dict["team"] as? String, points: dict["points"] as? Int, jj: dict["games"] as? Int, dg: dict["score_diff"] as? Int)
        return stadisticModel
    }
}
