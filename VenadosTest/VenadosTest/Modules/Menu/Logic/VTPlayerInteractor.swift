//
//  VTPlayerInteractor.swift
//  VenadosTest
//
//  Created by Ingeniero de Software on 5/14/19.
//  Copyright © 2019 Dacode. All rights reserved.
//

import UIKit
import Alamofire

class VTPlayerInteractor: VTBaseInteractor {
    
    public func getPlayers(success: @escaping(([VTPlayerModel]) -> Void), errorResponse: @escaping (String) -> Void){
        let url  = VTApiUrlHelper.shared.urlGetPlayers
        let headers = self.getHeaders()
        
        Alamofire.request(url, method: .get, headers: headers).responseJSON { response in
            switch (response.result){
            case .success(let json):
                print(json)
                let statusCode = response.response?.statusCode
                let dictionary = json as? NSDictionary
                
                if statusCode == 200 {
                    let players = self.dictToPlayerArrayModel(dictionary!)
                    success(players)
                }else {
                    return errorResponse("Error")
                }
            case .failure(let error):
                errorResponse(error.localizedDescription)
            }
        }
    }
    
    private func dictToPlayerArrayModel(_ dict : NSDictionary) -> [VTPlayerModel] {
        var array = [VTPlayerModel]()
        let dictData = dict["data"] as? NSDictionary
        let dictTeam = dictData!["team"] as? NSDictionary
        let arrayPlayers = dictTeam!["centers"] as? NSArray
        for player in arrayPlayers! {
            if let dict = player as? NSDictionary {
                array.append(self.dictToPlayerModel(dict))
            }
        }
        return array
    }
    
    func dictToPlayerModel(_ dict : NSDictionary) -> VTPlayerModel{
        
        let playerModel = VTPlayerModel(name: dict["name"] as? String, firstSurname: dict["first_surname"] as? String, secondSurname: dict["second_surname"] as? String, position: dict["position"] as? String, birthday: nil, birthPlace: dict["birth_place"] as? String, weight: dict["weight"] as? Double, height: dict["height"] as? Double, lastTeam: dict["last_team"] as? String, image: dict["image"] as? String)
        return playerModel
    }
}
